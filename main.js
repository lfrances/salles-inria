let debugify = (f, n='noname') => ((...inputs) => (console.log('[dbg:'+n+']', ...inputs), f(...inputs)));
let isRoom = x => {
    // let dd = x.querySelector('title');
    // debugger;
    return (x.querySelector('title')||{}).innerHTML == 'room';
};

// isRoom = debugify(isRoom);

let mkMail = x => `rennes-salle_${x.id.replace('__', '')}@zimbra-local.inria.fr`;
let mailToURL = x => `/proxy/https://zimbra.inria.fr/home/${x}/calendar`;

let toDate = x => {
    x = x.split('');
    [13,11,6,4].map(i => x.splice(i, 0, i > 10 ? ':' : '-'));
    return new Date(x.join(''));
};

document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
	locale: 'fr',
	plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
	selectable: true,
	defaultView: 'timeGridWeek',
	header: {
	    left: 'prev,next today',
	    center: 'title',
	    right: 'timeGridWeek,dayGridMonth,timeGridDay'
	},
	select: update.bind(null, 'set-range')
    });

    calendar.render();
});

let processMe = () => {
    askRange(...Array.from(document.querySelectorAll('input[type=\'datetime-local\']')).map(x => new Date(x.value)));
};

let stringifyGets = g => (g.length ? '?' : '')
    + Object.keys(g).map(k => k+'='+g[k]).join('&');

let parseFreeBusy = s => {
    let r, re = /FREEBUSY;FBTYPE=BUSY:([\dTZ]+)\/([\dTZ]+)/g;
    let events = [];
    while((r = re.exec(s)))
	events.push(r.slice(1).map(toDate));
    return events;
};

let getAvailability = async (room, tStart, tEnd, auth) => {
    let headers = new Headers();
    let getParams = {start: +tStart, end: +tEnd};
    if(auth.type == 'basic'){
	let str = btoa(auth.username + ":" + auth.password);
	headers.set('Authorization', 'Basic ' + str);
	getParams.auth = 'ba';
    }else
	throw "auth.type '"+auth.type+"' is not supported";
    
    let url = mailToURL(room.name) + '.ifb?' + stringifyGets(getParams);
    room.htmlUrl = mailToURL(room.name) + '?' + stringifyGets({start: +tStart, end: +tEnd, fmt: 'html'});
    console.log(url, {headers});
    return parseFreeBusy(await (await fetch(url, {headers})).text());
};

let $ = q => document.querySelector(q);
let $$ = q => Array.from(document.querySelectorAll(q));

let state = {
    tab: 'calendar-box'
};
let update = (message, data) => {
    if(message == "set-tab")
	state.tab = data;
    else if(message == 'load-auth-from-form'){
	let [username, password] = $$('.l').map(x => x.value);
	state.auth = {username, password, type: 'basic'};
    }else if(message == 'localstorage-save-auth'){
	let {username, password} = state.auth;
	localStorage.setItem('username', username);
	localStorage.setItem('password', password);
	console.log(password);
    }else if(message == 'localstorage-load-auth'){
	let auth = {
	    type: 'basic',
	    username: localStorage.getItem('username'),
	    password: localStorage.getItem('password')
	};
	if(auth.username && auth.password)
	    state.auth = auth;
    }else if(message=='set-range'){
	state.range = [data.start, data.end];
	askRange(data.start, data.end);
	update('set-tab', 'maps');
    }
    $$('#tabs > section').forEach(o => {
	let isMe = o.id == `button-${state.tab}`;
	o.className = isMe ? 'active' : '';
	$('#'+o.id.replace(/^button-/, ''))
	    .className = isMe ? '' : 'hide';
    });
    $('#status-info').innerText = (status.range || [])
	.map(r => r)
	.join('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
};
let init = () => {
    $$('#tabs > section').forEach(o => {
	o.onclick = () => update('set-tab',
				 o.id.replace(/^button-/, ''));
    });
    update('set-tab', 'calendar-box');
    update('localstorage-load-auth');
    whitify();
    setTimeout(whitify, 100);
};
document.addEventListener('DOMContentLoaded', init);

let whitify = () => $$('object').map(x => x.contentDocument).map(x => {
    let rooms = Array.from(x.getElementsByTagName('path')).filter(isRoom);
    rooms.map(room => {
	room.style.fill = '#e559ff';
	room.style.stroke = '#00a8ff';
	if(room.parentNode.tagName!='a'){
	    let el = x.createElementNS("http://www.w3.org/2000/svg", 'a');
	    let parent = room.parentNode;
	    parent.removeChild(room);
	    el.appendChild(room);
	    parent.appendChild(el);
	}
    });
});

let green = '#4cd137';
let red = '#e84118';
let askRange = async (tStart, tEnd) => {
    whitify();
    // let [username, password] = Array.from(document.querySelectorAll('input.l')).map(x => x.value);
    let r = $$('object').map(async d => {
	    d = d.contentDocument;
	    let rooms = Array.from(d.getElementsByTagName('path'))
		.filter(isRoom)
		.map(x => ({name: mkMail(x), domObj: x}));
	    for(let room of rooms){
		let events = await getAvailability(room, tStart, tEnd, state.auth);
		room.domObj.style.fill = events.length ? red : green;
		room.domObj.parentNode.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', room.htmlUrl.replace(/^.*https/, 'https')+'&view=week');
		room.domObj.parentNode.setAttributeNS('http://www.w3.org/1999/xlink', 'target', '_blank');
	    }
	});
    await Promise.all(r);
};

