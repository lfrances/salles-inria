# Salles INRIA

This tools show you on the map which rooms are availables given a time range.

## How to use
Requirements: `git`, `nodejs`, `npm` (or `yarn`).

 - Clone this repo `git clone https://gitlab.inria.fr/lfrances/salles-inria`
 - Install the required node packages `npm install`
 - Run the server `npm start`
 - Visit `http://localhost:8080/`
 - Type your login informations in the `Settings` page (icon in the top right corner)

## Screenshot

![](./demo.png?raw=true)

